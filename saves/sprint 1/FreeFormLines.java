package fr.tse.java.sprint1;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.RenderingHints.Key;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.nio.file.Files;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.AttributedCharacterIterator;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class FreeFormLines extends JFrame {
	
	
    public static void main(String[] args) {  	
        new FreeFormLines();
    }

    public FreeFormLines() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    ex.printStackTrace();
                }

                // rajouter le try catch pour io (gestion d'ouverture, d'ecrtiure et de fermeture des fichiers)
                
                TestPane pane = new TestPane(new GridBagLayout());
                GridBagConstraints c = new GridBagConstraints();
                
            	pane.setBackground(Color.white);
                
            	JButton bcolor, bred, bclear, bgom, bstrok,bstrok2, bcam, bmod, bsav,bop;
            	
            	bcolor = new JButton("color");
            	c.fill = GridBagConstraints.HORIZONTAL;
            	c.anchor = GridBagConstraints.PAGE_START;
                c.gridx = 0;
                c.gridy = 0;
                c.gridwidth = 1;
                pane.add(bcolor, c);
            	
                bred = new JButton("red");
            	c.fill = GridBagConstraints.HORIZONTAL;
            	c.anchor = GridBagConstraints.PAGE_START;
                c.gridx = 3;
                c.gridy = 3;
                c.gridwidth = 1;
                pane.add(bred, c);
                
                bgom = new JButton("gomme");
            	c.fill = GridBagConstraints.HORIZONTAL;
            	c.anchor = GridBagConstraints.PAGE_START;
                c.gridx = 1;
                c.gridy = 1;
                c.gridwidth = 1;
                pane.add(bgom, c);
                
                bstrok = new JButton("bigstrk");
            	c.fill = GridBagConstraints.HORIZONTAL;
            	c.anchor = GridBagConstraints.PAGE_START;
                c.gridx = 2;
                c.gridy = 2;
                c.gridwidth = 1;
                pane.add(bstrok, c);
                
                bstrok2 = new JButton("lilstrk");
            	c.fill = GridBagConstraints.HORIZONTAL;
            	c.anchor = GridBagConstraints.PAGE_START;
                c.gridx = 3;
                c.gridy = 2;
                c.gridwidth = 1;
                pane.add(bstrok2, c);
                
                bclear = new JButton("clear");
            	c.fill = GridBagConstraints.HORIZONTAL;
            	c.anchor = GridBagConstraints.PAGE_START;
                c.gridx = 4;
                c.gridy = 2;
                c.gridwidth = 1;
                pane.add(bclear, c);
                
                bsav = new JButton("save");
            	c.fill = GridBagConstraints.VERTICAL;
            	c.anchor = GridBagConstraints.SOUTH;
                c.gridx = 2;
                c.gridy = 2;
                c.gridwidth = 1;
                pane.add(bsav, c);
                
                bop = new JButton("open");
            	c.fill = GridBagConstraints.VERTICAL;
            	c.anchor = GridBagConstraints.SOUTH;
                c.gridx = 3;
                c.gridy = 2;
                c.gridwidth = 1;
                pane.add(bop, c);
                
                bcam = new JButton("CAMERA");
            	c.fill = GridBagConstraints.HORIZONTAL;
            	c.anchor = GridBagConstraints.PAGE_START;
                c.gridx = 4;
                c.gridy = 2;
                c.gridwidth = 1;
                
                bmod = new JButton("mode (type A)");
            	c.fill = GridBagConstraints.VERTICAL;
            	c.anchor = GridBagConstraints.SOUTH;
                c.gridx = 1;
                c.gridy = 1;
                c.gridwidth = 1;
                bmod.setActionCommand("mode");
                pane.add(bmod, c);
 
                bcolor.addActionListener(pane); // add to pane
                bred.addActionListener(pane);
                bgom.addActionListener(pane);
                bstrok.addActionListener(pane);
                bstrok2.addActionListener(pane);
                bclear.addActionListener(pane);
                bmod.addActionListener(pane);
                bsav.addActionListener(pane);
                bop.addActionListener(pane);
                
                JPanel weebcam = new JPanel(new GridBagLayout());
                GridBagConstraints weeb = new GridBagConstraints();
                weebcam.add(bcam, c);
                
                JFrame frame = new JFrame("Nyaaaa Drawing UwU");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                //frame.add(pane);
                //frame.add(weebcam);
                
                
                
                frame.add(pane, BorderLayout.WEST);
                frame.add(weebcam, BorderLayout.EAST);
               
                
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

    public class ColoredPoint{
        private List<List<Point>> hist;
        private Color col = Color.black;
        private BasicStroke strok =  new BasicStroke(1.0f);
        
        public ColoredPoint(List<List<Point>> line, Color c) {
        	hist = line;
        	col = c;
        }
        
        public ColoredPoint(List<List<Point>> line, Color c,BasicStroke st) {
        	hist = line;
        	col = c;
        	strok = st;
        }
        public void setColor(Color c) {
    		col = c;
    	}
    	
    	public Color getColor() {
    		return col;
    	}
    	
    	public void setStroke(BasicStroke st) {
    		strok = st;
    	}
    	
    	public BasicStroke getStroke() {
    		return strok;
    	}
    	
    	public void setPoint(List<List<Point>> dot) {
         	hist = dot;
        }
    	 
    	public List<List<Point>> getPoint() {
         	return hist;
        }
    	
    	@Override
    	public String toString() {
    		//en dessous toString pour voir le contenu r�sum�
    		//return("color : "+col.toString()+" number of points : "+ hist.size());
    		
    		return(col.toString()+" "+ strok.toString()+" "+hist.toString());
    	}
    	
    	
    }
    
    public class TestPane extends JPanel implements ActionListener {
    	
    	private Color color = Color.black;

    	public void setColor(Color col) {
    		color = col;
    	}
    	
    	public Color getColor() {
    		return color;
    	}
    	
    	private ArrayList<ColoredPoint> meow;
    	
    	public ArrayList<ColoredPoint> getMeow(){
    		return meow;
    	}
    	
    	public void setMeow(ArrayList<ColoredPoint> load) {
    		meow = load;
    	}
    	
        private List<List<Point>> points;
        
        public void setPoints(List<List<Point>> dot) {
        	points = dot;
        }
        
        private BasicStroke strok = new BasicStroke();
        
        public BasicStroke getStroke() {
        	return strok;
        }
        
        public void setStroke(BasicStroke st) {
        	strok = st;
        }
        
        
        public TestPane(GridBagLayout lay) {        	
        	points = new ArrayList<>(25);
        	meow = new ArrayList<FreeFormLines.ColoredPoint>(10);
        	meow.add(new ColoredPoint(points,Color.black));
            MouseAdapter ma = new MouseAdapter() {

                private List<Point> currentPath;

                @Override
                public void mousePressed(MouseEvent e) {
                    currentPath = new ArrayList<>(25);
                    currentPath.add(e.getPoint());

                    points.add(currentPath);
                }

                @Override
                public void mouseDragged(MouseEvent e) {
                    Point dragPoint = e.getPoint();
                    currentPath.add(dragPoint);
                    repaint();
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    currentPath = null;
                }

            };

            addMouseListener(ma);
            addMouseMotionListener(ma);
            
        }


        @Override
        public Dimension getPreferredSize() {
            return new Dimension(500, 500);
        }

        // a partir de la le This refere a un objet de type Testpane
        
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g.create();
            System.out.println(meow.toString());
            //System.out.println(points.toString());
            for (ColoredPoint lines : meow) {
            
	            for (List<Point> path : lines.getPoint()) {
	            	g2d.setColor(lines.getColor());
	            	g2d.setStroke(lines.getStroke());
	                Point from = null;
	                for (Point p : path) {
	                    if (from != null) {
	                        g2d.drawLine(from.x, from.y, p.x, p.y);
	                    }
	                    from = p;
	                }
	            }
            }
            // sale ici :
            for (List<Point> linesbis : points) {
            	Point frombis = null;
            	for (Point pathbis : linesbis) {
            		g2d.setColor(this.getColor());
        			g2d.setStroke(this.getStroke());
            		if (frombis != null) {
            			g2d.drawLine(frombis.x, frombis.y, pathbis.x, pathbis.y);
            		}
            		frombis = pathbis;
            		}
            }
            g2d.dispose();
        }
        
        // a refaire comme le repaint marche
        public void resetGraph(Graphics g) {
        	 super.paintComponent(g);
             Graphics2D g2d = (Graphics2D) g.create();
             g2d.setColor(Color.white);
             this.setPoints(points = new ArrayList<>(25));
             meow = new ArrayList<FreeFormLines.ColoredPoint>(10);
         	 meow.add(new ColoredPoint(points,Color.white));
             g2d.dispose();
             repaint();
        }
        
        
		@Override
		public void actionPerformed(ActionEvent e) {
        	String s = e.getActionCommand();
        	Color colorAction = this.getColor();
        	BasicStroke strokeAction = this.getStroke();
        	if (s == "save") {
        		BufferedImage bi = ScreenImage.createImage(this);
        		try {
					ScreenImage.writeImage(bi);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
        	}
        	else if (s == "open") {
        		try {
        			BufferedImage myPicture = ImageIO.read(new File("./images/black unicorn.png"));
        			
        			
        			
        			JLabel picLabel = new JLabel();
        			ImageIcon icon = new ImageIcon(myPicture);
        			picLabel.setIcon(icon);
        			
        			System.out.println(myPicture);
        			add(picLabel);
        			//pack();
        			//setContentPane(picLabel);
        			 
        			 
        			setVisible(true);

        		} catch (IOException e1) {
        			e1.printStackTrace();
        		}

        	}
        	else if (s == "mode") {
        		
        	}
        	else if (s == "color")
				{this.setColor(Color.black);} 
        	else if (s == "red")
				{this.setColor(Color.red);}
        	else if (s == "gomme")
        		{this.setColor(Color.white);
        		//this.setStroke(new BasicStroke(5.0f));
        		}
        	else if (s == "bigstrk"){
        		this.setStroke(new BasicStroke(5.0f));
        	}
        	else if (s == "lilstrk"){
        		this.setStroke(new BasicStroke(1.0f));
        	}
        	else
        		{resetGraph(getGraphics());}
        	ColoredPoint nyaaa = new ColoredPoint(points,colorAction,strokeAction);
        	meow.add(nyaaa);
        	points = new ArrayList<>(25);
		}
		
		public void saveAs(String name) {
			String fileName = name + ".txt";
			/*
			try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "utf-8"))) {
				writer.write("something");
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			*/
        	
			
			// list<String> toWrite = getMeow().toListString();
			Charset utf8 = StandardCharsets.UTF_8;
			List<String> lines = Arrays.asList("1st line", "2nd line");

			try {
			    Files.write(Paths.get("file3.txt"), "content".getBytes(utf8));
			    Files.write(Paths.get("file4.txt"), "content".getBytes(utf8));
			    Files.write(Paths.get("file5.txt"), lines, utf8);
			   // Files.write(Paths.get("file6.txt"), lines, utf8,StandardOpenOption.CREATE, StandardOpenOption.APPEND);
			} catch (IOException e) {
			    e.printStackTrace();
			}
			
        }
		

    }

}