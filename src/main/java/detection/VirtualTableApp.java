package detection;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;


public class VirtualTableApp extends Application{
    
	private static Stage stage;
	//getter
    public static Stage getStage() {
        return stage;
    }

    @Override
    public void start(Stage primaryStage)
    {
        try
        {
            // load the FXML resource
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/VirtualTable.fxml"));
            // store the root element so that the controllers can use it
            AnchorPane rootElement = (AnchorPane) loader.load();
        
            // create and style a scene
            Scene scene = new Scene(rootElement, 1200, 600);
            
            // create the stage with the given title and the previously created
            // scene
            primaryStage.setTitle("Virtual Board");
            primaryStage.setScene(scene);
            // show the GUI
            primaryStage.show();
            
            // set the proper behavior on closing the application
            VirtualTableController controller = loader.getController();
            primaryStage.setOnCloseRequest((new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we){
                    controller.setClosed();
                }
            }));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // launch the app
    public static void main(String[] args){
    	// load the native OpenCV library
    	nu.pattern.OpenCV.loadLocally();
        launch(args);
    }
}
