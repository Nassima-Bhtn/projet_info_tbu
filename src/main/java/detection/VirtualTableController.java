package detection;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.videoio.VideoCapture;


import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;


// the controller class 
public class VirtualTableController{
	// the FXML button
	@FXML
	private Button button;
	// the FXML image view
	@FXML
	private ImageView currentFrame;
	@FXML
	private ImageView maskImage;
	@FXML
	private ImageView morphImage;
	//the FXML canvas
	@FXML
    private Canvas canvas;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private CheckBox eraser;
    @FXML
    private ScrollBar brushSize;
	@FXML
	private Slider hueStart;
	@FXML
	private Slider hueStop;
	@FXML
	private Slider saturationStart;
	@FXML
	private Slider saturationStop;
	@FXML
	private Slider valueStart;
	@FXML
	private Slider valueStop;
	@FXML
	private Label label;
	@FXML
	private Button clearbutton;
	@FXML
    private Label MinHue;
    @FXML
    private Label MaxHue;
    @FXML
    private Label MinSat;
    @FXML
    private Label MaxSat;
    @FXML
    private Label MinB;
    @FXML
    private Label MaxB;

	// a timer for acquiring the video stream
	private ScheduledExecutorService timer;
	// the OpenCV object that realizes the video capture
	private VideoCapture capture = new VideoCapture();
	// a flag to change the button behavior
	private boolean cameraActive = false;
	// the id of the camera to be used
	private static int cameraId = 0;
	// variable to store the detected points from the camera
	private List<Point> Points = new ArrayList<>();
	// variable to get the pixel value under the cursor
	PixelReader pixelReader;
	
	/**
	 * The action triggered by pushing the button on the GUI
	 * @param event
	 */

	@FXML
	protected void startCamera(ActionEvent event)
	{	
		
		if (!this.cameraActive){

			// start the video capture
			this.capture.open(cameraId);
			
			// is the video stream available?
			if (this.capture.isOpened()){
				this.cameraActive = true;
				
				// grab a frame every 33 ms (30 frames/sec)
				Runnable frameGrabber = new Runnable() {
					
					@Override
					public void run(){
						// effectively grab and process a single frame
						Mat frame = grabFrame();
						// convert and show the frame
						Image imageToShow = Utils.mat2Image(frame);
						updateImageView(currentFrame, imageToShow);
					}
				};
				
				this.timer = Executors.newSingleThreadScheduledExecutor();
				this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);
				
				// update the button content
				this.button.setText("Stop Camera");
			}
			else{
				// log the error
				System.err.println("Impossible to open the camera connection...");
			}
		}else{
			// the camera is not active at this point
			this.cameraActive = false;
			// update again the button content
			this.button.setText("Start Camera");
			
			// stop the timer
			this.stopAcquisition();
		}
	}
	
	
	 // Get a frame from the opened video stream 
	 // all the image processing is done here

	private Mat grabFrame(){
		// initialize the frame
		Mat frame = new Mat();
		
		// check if the capture is open
		if (this.capture.isOpened()){
			try{
				// read the current frame
				this.capture.read(frame);
				
				// if the frame is not empty, process it
				if (!frame.empty()){
					
		//code for object detection 
					
					// to get the value of a pixel on click
					currentFrame.setOnMouseClicked(mouseHandler);
					Image imageFrme = Utils.mat2Image(frame);
					pixelReader = imageFrme.getPixelReader();
					
					// initialize variables 
					Mat blurredImage = new Mat();
					Mat hsvImage = new Mat();
					Mat mask = new Mat();
					Mat morphOutput = new Mat();	
					
					// blur the image
					Imgproc.blur(frame, blurredImage, new Size(7, 7));
					
					// convert the image to hsv
					Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);
					
					// get threshold 
					Scalar minValues = new Scalar(this.hueStart.getValue(), this.saturationStart.getValue(),
							this.valueStart.getValue());
					Scalar maxValues = new Scalar(this.hueStop.getValue(), this.saturationStop.getValue(),
							this.valueStop.getValue());
					
					// threshold HSV image to select object
					Core.inRange(hsvImage, minValues, maxValues, mask);
					
					// show the partial output
					Core.flip(mask, mask, +1);
					this.updateImageView(this.maskImage, Utils.mat2Image(mask));
					
					
					// morphological operators
					// get the gradiant  
					Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(24, 24));
					Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(12, 12));
					
					Imgproc.erode(mask, morphOutput, erodeElement);
					Imgproc.erode(morphOutput, morphOutput, erodeElement);
					
					Imgproc.dilate(morphOutput, morphOutput, dilateElement);
					Imgproc.dilate(morphOutput, morphOutput, dilateElement);
					
					// show the partial output
					this.updateImageView(this.morphImage, Utils.mat2Image(morphOutput));
					
					// flip the output to facilitate the drawing 
					Core.flip(morphOutput, morphOutput, +1);
					
					// find the center of the detected object
					frame = this.find(morphOutput, frame);
					
					// draw on the canvas and dirrectly on the frame
					frame = this.drawLine( frame);
				}
				
			}
			catch (Exception e)
			{
				// log the error
				System.err.println("Exception during the image elaboration: " + e);
			}
		}
		
		// flip the vid�o the be in cordination with the drawing
		Core.flip(frame, frame, +1);
		return frame;
	}
	
	
	
	//  find and draw the center of object 
	private Mat find(Mat maskedImage, Mat frame){
		
		// initialize
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		
		// find contours
		Imgproc.findContours(maskedImage, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
		
		// if any contour exist...
		List<Point> stockage = new ArrayList<>(contours.size());
		if (hierarchy.size().height > 0 && hierarchy.size().width > 0){
			// for each contour, display it in yellow
			for (int idx = 0; idx >= 0; idx = (int) hierarchy.get(0, idx)[0]){
				Imgproc.drawContours(frame, contours, idx, new Scalar(0, 250, 250));

				// find the center
				List<Moments> mu = new ArrayList<>(contours.size());
				mu.add(Imgproc.moments(contours.get(idx)));
				Point point = new Point(mu.get(0).m10 / (mu.get(0).m00 + 1e-5), mu.get(0).m01 / (mu.get(0).m00 + 1e-5));
				stockage.add(point);
				Points.add(point);
			}
		}else {	
			// remove the drawing from the frame once the object is no longer detected 
			Points.clear();
		}
		
		//draw a little red dot on the detected point
		for (int i = 0; i < contours.size(); i++) {
			Imgproc.circle(frame, stockage.get(i), 5 ,new Scalar(0, 0, 250),5);						   	 
		}
		return frame;	
	}
	
	// draw a line between the detected points
	private Mat drawLine(Mat frame){
		
		for (int i = 0; i < Points.size()-1; i++) {
			GraphicsContext g = canvas.getGraphicsContext2D();
			double size = brushSize.getValue();
            double x = canvas.getWidth() - Points.get(i).x - size / 2;
            double y = Points.get(i).y - size / 2;
            
            if (eraser.isSelected()) {
            	// remove the drawn pixel
                g.setFill(Color.WHITE);
                g.fillRect(x, y, size, size);
            } else {
            	// draw with the selected color 
                g.setFill(colorPicker.getValue());
                g.fillRect(x, y, size, size);
            }      
     // to draw on the camera frame        
     // Imgproc.line ( frame, Points.get(i), Points.get(i+1),  new Scalar(0, 0, 255), 5);
		}
		return frame;
	}
	
	// draw on the canvas with the mouse
	public void initialize() {
        GraphicsContext g = canvas.getGraphicsContext2D();
        g.setFill(Color.WHITE);
        g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        	canvas.setOnMouseDragged(e -> {
               double size = brushSize.getValue();
               double x = e.getX() - size / 2;
               double y = e.getY() - size / 2;
               if (eraser.isSelected()) {
                  // g.clearRect(x, y, size, size);
                   g.setFill(Color.WHITE);
                   g.fillRect(x, y, size, size);
               } else {
                   g.setFill(colorPicker.getValue());
                   g.fillRect(x, y, size, size);
}
           });
       }
	
	
	// set the configuration to visible when a button is clicked
    public void config() {
        if (morphImage.getOpacity()==0){
            hueStart.setOpacity(100);
            hueStop.setOpacity(100);
            saturationStart.setOpacity(100);
            saturationStop.setOpacity(100);
            valueStart.setOpacity(100);
            valueStop.setOpacity(100);
            morphImage.setOpacity(100);
            maskImage.setOpacity(100);
            MinHue.setOpacity(100);
            MaxHue.setOpacity(100);
            MinSat.setOpacity(100);
            MaxSat.setOpacity(100);
            MinB.setOpacity(100);
            MaxB.setOpacity(100);
        }else {
            hueStart.setOpacity(0);
            hueStop.setOpacity(0);
            saturationStart.setOpacity(0);
            saturationStop.setOpacity(0);
            valueStart.setOpacity(0);
            valueStop.setOpacity(0);
            morphImage.setOpacity(0);
            maskImage.setOpacity(0);
            MinHue.setOpacity(0);
            MaxHue.setOpacity(0);
            MinSat.setOpacity(0);
            MaxSat.setOpacity(0);
            MinB.setOpacity(0);
            MaxB.setOpacity(0);
        }    
    }
	
	
	// function to delete all the drawing 
	public void clearfunction() {
		GraphicsContext g = canvas.getGraphicsContext2D();
		//g.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        g.setFill(Color.WHITE);
        g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
	}
	
	
	// save the drawing to a png file 
	public void onSave() {
		
                FileChooser fileChooser = new FileChooser();
                //Set extension filter
                FileChooser.ExtensionFilter extFilter = 
                        new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
                fileChooser.getExtensionFilters().add(extFilter);
              
                //Show save file dialog
                File file = fileChooser.showSaveDialog( VirtualTableApp.getStage());
                
                if(file != null){
                    try {
                        WritableImage writableImage = new WritableImage( (int) canvas.getWidth(), (int) canvas.getHeight());
                        canvas.snapshot(null, writableImage);
                        RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                        ImageIO.write(renderedImage, "png", file);
                    } catch (IOException ex) {
                        Logger.getLogger(VirtualTableController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            
	}
	
	// open a saved drawing 
	public void onOpen() {
		// choose the file in question
		FileChooser openFile = new FileChooser();
		GraphicsContext g = canvas.getGraphicsContext2D();
        openFile.setTitle("Open File");
        File file = openFile.showOpenDialog( VirtualTableApp.getStage());
        if (file != null) {
            try {
                InputStream io = new FileInputStream(file);
                Image img = new Image(io);
                g.drawImage(img, 0, 0);
            } catch (IOException ex) {
                System.out.println("Error!");
            }
        }
	}
	
	// get the pixel color on click
	EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent e) {
//			double[] c;
//	        int color = pixelReader.getArgb((int) e.getX(), (int) e.getY());
//	        int red = (color >> 16) & 0xff;
//	        int green = (color >> 8) & 0xff;
//	        int blue = color & 0xff;
		
            Color color1 = pixelReader.getColor((int) e.getX(), (int) e.getY());
            double hue = color1.getHue();
            //double saturation = color1.getSaturation();
            //double value = color1.getBrightness();
            
            // Modify the curser value to the color selected 
            hueStart.setValue(Math.abs(hue)-50);
            hueStop.setValue(hue+30);
		
		
		
		}
	
	};       
	        
	
	// Stop the acquisition from the camera and release all the resources
	 
	private void stopAcquisition(){
		if (this.timer!=null && !this.timer.isShutdown()){
			try{
				// stop the timer
				this.timer.shutdown();
				this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
			}
			catch (InterruptedException e){
				// log any exception
				System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
			}
		}
		
		if (this.capture.isOpened()){
			// release the camera
			this.capture.release();
		}
	}
	
	// Update the {@link ImageView} in the JavaFX main thread
	private void updateImageView(ImageView view, Image image){
		Utils.onFXThread(view.imageProperty(), image);
	}
	
	
	// On application close, stop the acquisition from the camera
	protected void setClosed(){
		this.stopAcquisition();
	}	
}