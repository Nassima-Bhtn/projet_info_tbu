package detection;

import java.io.ByteArrayInputStream;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;

import javafx.scene.image.Image;

public final class Utils {
	
	// convert a matrix to an image
	public static Image mat2Image(Mat frame){
            // créer le buffer
            MatOfByte buffer = new MatOfByte();
            // encoder le frame dans le buffer
            Imgcodecs.imencode(".png", frame, buffer);
            // construire et renvoyer une image crée à partir de l'image encodée dans le buffer 
            return new Image(new ByteArrayInputStream(buffer.toArray()));
    }

	// update the thread
    public static <T> void onFXThread(final ObjectProperty<T> property, final T value){
        Platform.runLater(() -> {
            property.set(value);
        });
    }

}